package Lecture13;

import Work.Employee;
import Work.Company;
import java.util.Scanner;

public class TestProgram {
    public static void main(String[] args) {
        Employee anna = new Employee("Анна");
        Employee alexey = new Employee("Алексей");
        Employee maxim = new Employee("Максим");

        Employee dima = new Employee("Дмитрий");
        Employee liza = new Employee("Елизавета");
        Employee denis = new Employee("Денис");

        Employee marina = new Employee("Марина");
        Employee olesya = new Employee("Олеся");
        Employee katya = new Employee("Екатерина");

        Employee laura = new Employee("Лаура");
        Employee adam = new Employee("Адам");
        Employee victor = new Employee("Виктор");

        anna.setPosition("тестировщик");
        anna.setSalary(80);
        alexey.setPosition("разработчик");
        alexey.setSalary(120);
        maxim.setPosition("руководитель");
        maxim.setSalary(200);
        dima.setPosition("инженер");
        dima.setSalary(90);
        liza.setPosition("офис-менеджер");
        liza.setSalary(100);
        denis.setPosition("директор");
        denis.setSalary(180);
        marina.setPosition("переводчик");
        marina.setSalary(60);
        olesya.setPosition("аналитик");
        olesya.setSalary(250);
        katya.setPosition("CEO");
        katya.setSalary(140);

        Employee[] Employees = new Employee[]{marina, olesya, katya, dima, liza, denis, anna, alexey, maxim};

        Employee[] EmployeesTSystems = new Employee[]{marina, olesya, katya};
        Employee[] EmployeesJetBrains = new Employee[]{dima, liza, denis};
        Employee[] EmployeesDell = new Employee[]{anna, alexey, maxim};

        Company tSystems = new Company("T-Systems", "Germany, Bonn, Landgrabenweg, 1", "IT", EmployeesTSystems);
        Company jetBrains = new Company("JetBrains", "Czech Republic, Prague, Karlov square", "IT", EmployeesJetBrains);
        Company dell = new Company("Dell", "USA, Texas, Ostin, Liberty str.", "IT", EmployeesDell);

        //вспомогательный метод (должен быть доступен только в самом классе),
        // чтобы по должности была возможность найти сотрудника (предположим, что в наших компаниях должности не повторяются).
        // Scanner sc = new Scanner(System.in);
        // System.out.println("Введите должность: ");
        //  String s = sc.nextLine();
        // Company.findEmployee(s, Employees);
        System.out.println();

        //метод чтобы получить владельца компании (эта информация должны быть доступна только в классах-наследниках)
        tSystems.owner = laura;
        jetBrains.owner = adam;
        dell.owner = victor;

        // Company.findOwner(tSystems);
        System.out.println();

        //общедоступный метод, чтобы узнать кто руководит компанией (назвать можно и генеральным директор, и CEO, как вам удобнее).
        Company.findCeo(jetBrains);

       // Вывести на экран максимальную и среднюю зарплату во всех компаниях.
        Employee.maxSalary(tSystems);
        System.out.println();
        Employee.maxSalary(jetBrains);
        System.out.println();
        Employee.maxSalary(dell);
        System.out.println();

        Employee.averageSalary(tSystems);
        System.out.println();
        Employee.averageSalary(jetBrains);
        System.out.println();
        Employee.averageSalary(dell);
    }

}

