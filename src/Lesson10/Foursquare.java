package Lesson10;

public class Foursquare {
    public static double perimeter(double side) {
        double x = side * 4;
        System.out.println("Периметр квадрата: " + x);
        return x;
    }

    public static double square(double side) {
        double s = Math.pow(side, 2);
        System.out.println("Площадь квадрата: " + s);
        return s;
    }
}

