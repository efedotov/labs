package Lesson10;

public class Rectangle {

    public static double perimeter(double width, double length) {
        double x = width * 2 + length * 2;
        System.out.println("Периметр прямоугольника: " + x);
        return x;
    }

    public static double square(double width, double length) {
        double s = width * length;
        System.out.println("Площадь прямоугольника: " + s);
        return s;
    }
}

