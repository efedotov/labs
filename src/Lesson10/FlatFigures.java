package Lesson10;

public class FlatFigures {
    public static void main(String[] args) {
        String turnFigure = turn("left", 50);

        Rectangle.perimeter(15, 20);
        Rectangle.square(15, 20);
        Foursquare.square(25);
        Foursquare.perimeter(25);
        Circle.circumference(3);
        Circle.square(3);
    }

    private static String turn(String direction, double grade) {
        return direction + grade;
    }
}

