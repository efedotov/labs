package Lesson10;

public abstract class Circle {

    public static double circumference(double radius) {
        double x = Math.PI * 2 * radius;
        System.out.println("Длина окружности: " + x);
        return x;
    }

    public static double square(double radius) {
        double s = Math.PI * Math.pow(radius, 2);
        System.out.println("Площадь окружности: " + s);
        return s;
    }
}

