package Exam;

public class Main {
    public static void main(String[] args) {
        Motorcycle moto1 = new Motorcycle(500, "Harley Davidson", false);
        Motorcycle moto2 = new Motorcycle(300, "Honda", true);
        Motorcycle moto3 = new Motorcycle(100, "Suzuki", true);

        PassengerCar car1 = new PassengerCar(1000, "Subaru", true);
        PassengerCar car2 = new PassengerCar(20, "Zaporozhetz", false);

        Driver driver1 = new Driver("Aнтон", "A");
        Driver driver2 = new Driver("Андрей", "AB");

        Motorcycle moto[] = {moto1, moto2, moto3};
        PassengerCar car[] = {car1, car2};
        Vehicle vehicle[] = {moto1, moto2, moto3, car1, car2};

        //Задание№1
        System.out.println("Old version: Список доступных ТС");
        Company.listAvailableVehiclesOldV(driver1, moto, car);
        System.out.println();
        System.out.println("New version: Список доступных ТС");
        driver1.listAvailableVehicles(moto, car);
        System.out.println();
        driver2.listAvailableVehicles(moto, car);
        System.out.println();

        //Задание№2
        System.out.println("Old version: Список опций ТС"); // выводит все ТС заданной категории, независимо от того, с каким вызываем
        moto1.listOfOptionsOldV(moto, car, moto1);
        moto2.listOfOptionsOldV(moto, car, moto1);
        System.out.println();
        System.out.println("New version: Список опций ТС"); // выводит для заданного ТС, но пришлось добавить в parent class Vehicle методы для isSidecar, isConditioning
        vehicle[2].listOfOptions(moto, car, moto1);
        vehicle[0].listOfOptions(moto,car, moto1);
        vehicle[4].listOfOptions(moto,car, moto1);
        System.out.println("Exam version: Список опций ТС");
        Vehicle.qwerty(car1);
    }


}
