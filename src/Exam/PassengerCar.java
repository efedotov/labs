package Exam;

public class PassengerCar extends Vehicle {
    boolean conditioning;

    PassengerCar(int price, String brand, boolean conditioning) {
        super(price, brand);
        this.conditioning = conditioning;
    }

    public boolean isConditioning() {
        return conditioning;
    }
}


