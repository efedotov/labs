package Exam;

public class Vehicle {
    int price;
    String brand;
    boolean sidecar;
    boolean isConditioning;

    Vehicle(int price, String brand) {
        this.price = price;
        this.brand = brand;
    }

    public int getPrice() {
        return price;
    }

    public String getBrand() {
        return brand;
    }

    public boolean isSidecar() {
        return sidecar;
    }

    public boolean isConditioning() {
        return isConditioning;
    }

    // Old version
    public void listOfOptionsOldV(Motorcycle moto[], PassengerCar car[], Motorcycle moto1) {
        if (getClass().equals(moto1.getClass())) {
            for (Motorcycle element : moto) {
                System.out.println(element.getBrand() + ", " + element.getPrice() + ", " + element.isSidecar());
            }
        } else
            for (PassengerCar element : car) {
                System.out.println(element.getBrand() + ", " + element.getPrice() + ", " + element.isConditioning());
            }
    }

    // New version
    public void listOfOptions(Motorcycle moto[], PassengerCar car[], Motorcycle moto1) {
        System.out.print(getBrand() + ", " + getPrice() + ", ");
        if (getClass().equals(moto1.getClass())) {
            System.out.println(isSidecar());
        } else
        System.out.println(isConditioning());
    }

    // Exam version
    public static void qwerty(Vehicle vehicle) {
        System.out.print(vehicle.getBrand() + ", " + vehicle.getPrice() + ", ");
        if (vehicle instanceof Motorcycle) {
            System.out.println(((Motorcycle) vehicle).isSidecar());
        } else {
            System.out.println(((PassengerCar) vehicle).isConditioning());
        }
    }


}
