package Exam;

public class Driver {
    Motorcycle motorcycle;
    PassengerCar passengerCar;
    String driverLicense;
    String name;

    public Driver(String name, String driverLicense) {
        this.name = name;
        this.driverLicense = driverLicense;
    }

    public String getDriverLicense() {
        return driverLicense;
    }

    public String getName() {
        return name;
    }

    public Motorcycle listAvailableMotorcycles(Motorcycle moto[]) {
        for (Motorcycle element : moto) {
            System.out.println(element.getBrand() + ", " + element.getPrice() + ", " + element.isSidecar());
        }
        return motorcycle;
    }

    public PassengerCar listAvailableCar(PassengerCar car[]) {
        for (PassengerCar element : car) {
            System.out.println(element.getBrand() + ", " + element.getPrice() + ", " + element.isConditioning());
        }
        return passengerCar;
    }

    public void listAvailableVehicles(Motorcycle moto[], PassengerCar car[]) {
        System.out.println(getName() + ", Вам доступны:");
        listAvailableMotorcycles(moto);
        if (getDriverLicense().equals("AB")) {
            listAvailableCar(car);
        }
    }

}
