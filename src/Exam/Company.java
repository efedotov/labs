package Exam;

public class Company {
    Motorcycle motorcycle;
    PassengerCar passengerCar;

    // Old version
    public static void listAvailableVehiclesOldV (Driver driver, Motorcycle moto[], PassengerCar car[]) {
        System.out.println(driver.getName() + ", Вам доступны:");
        for (Motorcycle element : moto) {
            System.out.println(element.getBrand() + ", " + element.getPrice() + ", " + element.isSidecar());
        }
        if (driver.getDriverLicense().equals("AB")) {
            for (PassengerCar element : car) {
                System.out.println(element.getBrand() + ", " + element.getPrice() + ", " + element.isConditioning());
            }
        }
    }


    // New methods, cannot be called with driver1/driver2 in Main
//    public Motorcycle listAvailableMotorcycles(Motorcycle moto[]) {
//        for (Motorcycle element : moto) {
//            System.out.println(element.getBrand() + ", " + element.getPrice() + ", " + element.isSidecar());
//                        } return motorcycle;
//    }

//        public PassengerCar listAvailableCar (PassengerCar car[]){
//            for (PassengerCar element : car) {
//                System.out.println(element.getBrand() + ", " + element.getPrice() + ", " + element.isConditioning());
//            }
//            return passengerCar;
//        }
}
