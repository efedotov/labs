package Exam;

public class Motorcycle extends Vehicle {
     boolean sidecar;

    Motorcycle(int price, String brand, boolean sidecar) {
        super(price, brand);
        this.sidecar = sidecar;
    }

    public boolean isSidecar() {
        return sidecar;
    }


}
