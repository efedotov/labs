package Lesson11_final;

public class Address {
    String country;
    String city;
    String street;
    int house;
    int flat;
    int postalCode;

    Address(String country, String city, String street, int house, int flat, int postalCode) {
        this.country = country;
        this.city = city;
        this.street = street;
        this.house = house;
        this.flat = flat;
        this.postalCode = postalCode;
    }

    Address(String country, String city, String street, int house, int flat) {
        this.country = country;
        this.city = city;
        this.street = street;
        this.house = house;
        this.flat = flat;
    }

    public void printInfo () {
        System.out.println(country + "\t city: " + city + "\t street:" + street + "\t house:" + house + "\t flat:" + flat);
    }

}

