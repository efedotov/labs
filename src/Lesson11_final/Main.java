package Lesson11_final;

public class Main {
    public static void main(String[] args) {
        // Создайте пару адресов и выведите их на экран
        Address vikaAddress = new Address("USA", "New York", "105-th Avenue", 25, 145, 123456);
        Address natashaAddress = new Address("Great Britain", "London", "Regent street", 16, 35);
        Address officeAddress = new Address("Great Britain", "London", "Trafalgar square", 10, 2);

        Person vika = new Person("Vika", 2000);
        Person natasha = new Person("Natasha", 2003);

        System.out.println("Адрес Вики: ");
        vikaAddress.printInfo();
        // или
        System.out.println();
        vika.myHome = vikaAddress;
        vika.myHome.printInfo();
        System.out.println();

        natasha.setMyHome(natashaAddress);
        natasha.setOfficeAddress(officeAddress);
        natasha.printAddresses();
        System.out.println();
        System.out.println();

        // Создайте одну или несколько семей
        Person mother = new Person("Анна", "Владимировна", 1980);
        Person father = new Person("Василий", "Евгеньевич", 1976);
        String[] children = new String[]{"Наташа", "Андрей", "Дина", "Ева"};
        Person dog = new Person("Майкл", 2015);
        mother.spouse = father;
        father.spouse = mother;
        Address myHome = new Address("Russia", "SPb", "Nevsky pr", 15, 56, 16589);

        Person.printFamily(mother, father, dog, myHome, children);
        System.out.println();
        System.out.println("Муж мамы " + mother.spouse.givenName);
        System.out.println("Жена папы " + father.getSpouse().givenName);
        System.out.println();

    }
}


