package Lesson11_final;

public class Person {
    final String familyName = "Прокопенко";
    String patronymic;
    final String givenName;
    int dateOfBirth;
    static Address myHome;
    static Address officeAddress;
    Person spouse;

    Person(String givenName, String patronymic, int dateOfBirth) {
        this.patronymic = patronymic;
        this.givenName = givenName;
        this.dateOfBirth = dateOfBirth;
    }

    Person(String givenName, int dateOfBirth) {
        this.givenName = givenName;
        this.dateOfBirth = dateOfBirth;
    }

    public static void printFamily(Person mother, Person father, Person dog, Address myHome, String[] children) {
        System.out.println("Семья");
        System.out.println("Мама: " + mother.givenName + "\t" + mother.patronymic + "\t" + mother.familyName + "\t" + mother.dateOfBirth);
        System.out.println("Папа: " + father.givenName + "\t" + father.patronymic + "\t" + father.familyName + "\t" + father.dateOfBirth);
        System.out.print("Дети: \t");

        for (String element : children) {
            System.out.print(element + ";\t");
        }

        System.out.println();
        System.out.println("Собака: " + dog.givenName + "\t" + dog.dateOfBirth);
        System.out.println("Адрес: " + myHome.country + ",\t" + myHome.city + ",\t" + myHome.street + ",\t" + myHome.house);
    }

    public Person getSpouse() {
        return spouse;
    }

    public void printAddresses() {
        System.out.println(givenName);
        System.out.println("Home address");
        myHome.printInfo();
        System.out.println("Office address");
        officeAddress.printInfo();
    }

    public void setOfficeAddress(Address officeAddress) {
        this.officeAddress = officeAddress;
    }

    public void setMyHome(Address myHome) {
        this.myHome = myHome;
    }
}