package Lesson5;

import java.util.Scanner;

public class Practice5 {
    public static void main(String[] args) {

        // задание 1
        // По номеру времени года вывести его название. Например, для 0 – вывести «зима», для 3 – «лето»
        System.out.println("Задание №1");
        System.out.println("Задайте число от 1 до 4:");
        Scanner scan = new Scanner(System.in);
        int i = scan.nextInt();
        switch (i) {
            case 1:
                System.out.println("Зима");
                break;
            case 2:
                System.out.println("Весна");
                break;
            case 3:
                System.out.println("Лето");
                break;
            case 4:
                System.out.println("Осень");
                break;
            default:
                System.out.println("Времен года всего четрые, нумерация начинается с 1:)");
        }
        System.out.println("");


        // задание 2
        // Даны 3 числа, a, b, c. Проверить истинность a < b < c. Вывести результат.
        // сложный способ
        System.out.println("Задание №2");
        int a = 7;
        int b = 8;
        int c = 9;
        if (a > b) {
            System.out.println("a<b<c false");
        } else if (b > c) {
            System.out.println("a<b<c false");
        } else {
            System.out.println("a<b<c true");
        }
        System.out.println("");

        // способ попроще
        int aa = 9;
        int bb = 8;
        int cc = 7;
        if (aa < bb) {
            if (bb < cc) {
                System.out.println("aa < bb < cc true");
            } else {
                System.out.println("aa < bb < cc false");
            }
        } else {
            System.out.println("aa < bb < cc false");
        }
        System.out.println("");

        // ещё попроще
        int a1 = 15;
        int b1 = 20;
        int c1 = 25;
        if (a1 < b1 && b1 < c1) {
            System.out.println("a1 < b1 < c1 true");
        } else {
            System.out.println("a1 < b1 < c1 false");
        }
        System.out.println("");


        // задание 3
        // Найти факториал числа (произведение всех чисел, от 1 и до числа. Например, факториал 5 = 1 * 2 * 3 * 4 * 5)
        // вариант как на лекции
        System.out.println("Задание №3");
        int d = 1;
        int f = 0;
        while (++f <= 7) d *= f;
        System.out.println("Факториал 7 равен: " + d);
        System.out.println("");

        // вариант с for
        int dd = 1;
        int ff;
        for (ff = 1; ff <= 5; dd *= ff++) ;
        System.out.println("Факториал 5 равен: " + dd);
        System.out.println("");


        // задание 4
        // Перевернуть цифры числа. Дано целое число, например 1234567. Вывести 7654321.
        System.out.println("Задание №4");
        int y = 12345;
        int num = (int) Math.log10(y) + 1;
        int z = 0;
        while (z < num) {
            System.out.print(y % 10);
            ++z;
            y = y / 10;
        }
        System.out.println("");
        System.out.println("");


        // задание 5
        System.out.println("Задание №5.1 - Среднее арифметическое десяти первых чисел, кратных трем");
        int ii = 0;
        double h = 0;
        double m;
        while (ii < 10) {
            ii++;
            m = 3 * ii;
            h += m;
        }
        System.out.println("arithmetical mean (multiple of three): " + h / 10);
        System.out.println("");

        System.out.println("Задание №5.2 - Среднее арифметическое десяти первых чисел - степеней тройки");
        int i1 = 1;
        double h1 = 0;
        double m1;
        while (i1 <= 10) {
            m1 = (double) Math.pow(3, i1);
            i1++;
            h1 += m1;
        }
        System.out.println("arithmetical mean (power of three) : " + h1 / 10);
        System.out.println("");

        System.out.println("Задание №5.2 - Среднее арифметическое десяти первых чисел - степеней тройки_оптимизированный вариант");
        int s = 1;
        int e;
        int q;
        double l = 0;
        for (e = 1; e < 11; e++) {
            q = 3 * s;
            l += q;
            s = q;
        }
        System.out.println("arithmetical mean (power of three) : " + l / 10);
        System.out.println("");


        // задание 6_1
        // Найти количество цифр заданного числа n типа long (например, 1000 – 4 цифры)
        System.out.println("Задание №6_1");
        int j = 1;
        for (long L = 500000000L; L / (int) Math.pow(10, j) > 0; ) {
            j++;
        }
        System.out.println("Amount: " + j);
        System.out.println("");


        // задание 6_2
        System.out.println("Задание №6_1 через логарифм:)");
        long number = 88888888;
        long Amount = (long) Math.log10(number) + 1;
        System.out.println("Amount: " + Amount);
        System.out.println("");


        // задание 7_1
        // Проверить, является ли число степенью двойки. Вывести «да» или «нет». По желанию вывести степень.
        System.out.println("Задание №7_1");
        System.out.print("Введите число: ");
        scan = new Scanner(System.in);
        double digit = scan.nextInt();
        int k = 1;
        long currentTime = System.nanoTime();

        if (digit < 2) {
            System.out.print("no");
        } else {
            if (digit == 2) {
                System.out.print("The digit is power of 2. Power = 1");
            } else {
                while (digit > 2) {
                    digit = digit / 2;
                    k++;
                    if (digit == 2) {
                        System.out.print("The digit is power of 2. Power = " + k);
                    } else if (digit % 1 != 0) {
                        System.out.print("no ");
                        break;
                    }
                }
            }
        }
        System.out.println("");
        System.out.println("Время выполнения Задание №7_1: \t" + (System.nanoTime() - currentTime));
        System.out.println("");
        System.out.println("");


        // задание 7_2
        System.out.println("Задание №7_2");
        System.out.print("Введите число: ");
        scan = new Scanner(System.in);
        double input_number = scan.nextInt();
        int t;
        long currentTime1 = System.nanoTime();

        if (input_number == 0) {
            System.out.print("no");
        } else if (input_number == 1) {
            System.out.print("The digit is power of 2. Power = 0");
        } else {
            for (t = 1; input_number / (int) Math.pow(2, t) > 1; t++) ;
            {
                if (input_number / (int) Math.pow(2, t) % 1 != 0) {
                    System.out.print("no");
                } else {
                    System.out.print("The digit is power of 2. Power = " + t);
                }
            }
        }
        System.out.println("");
        System.out.println("Время выполнения Задание №7_2: \t" + (System.nanoTime() - currentTime1));
        System.out.println("");


        // задание 7_3 - еще один вариант, более экономный, чем 7_2
        System.out.println("Задание №7_3");
        System.out.print("Введите число: ");
        scan = new Scanner(System.in);
        double given_number = scan.nextInt();
        double w;
        int power = 0;
        long currentTime2 = System.nanoTime();

        if (given_number == 0) {
            System.out.print("no");
        } else if (given_number == 1) {
            System.out.print("The digit is power of 2. Power = 0");
        } else {
            do {
                power++;
                w = given_number / Math.pow(2, power);
            } while (w > 1 && (w % 1 == 0));
            if (w % 1 != 0) {
                System.out.print("no");
            } else {
                System.out.print("The digit is power of 2. Power = " + power);
            }
        }

        System.out.println("");
        System.out.println("Время выполнения Задание №7_3: \t" + (System.nanoTime() - currentTime2));
        System.out.println("");
        System.out.println("");


        // задание 7_4 - через корень
        System.out.println("Задание №7_4 - через корень");
        System.out.print("Введите число: ");
        scan = new Scanner(System.in);
        double input = scan.nextInt();
        int p = 0;
        double v = 3;
        long currentTime3 = System.nanoTime();

        if (input == 1) {
            System.out.print("The digit is power of 2. Power = 0");
        } else {
            while (v > 2) {
                p++;
                v = Math.pow(input, 1.0 / p);
            }
            if (v == 2) {
                System.out.print("The digit is power of 2. Power = " + p);
            } else {
                System.out.print("no");
            }
        }

        System.out.println("");
        System.out.println("Время выполнения Задание №7_4: \t" + (System.nanoTime() - currentTime3));
        System.out.println("");


        // задание 8
        // Используя генератор случайных чисел (long rand = Math.round(Math.random() * макс_число)),
        // который генерирует целые числа от 0 до макс_число, написать генератор супергеройских имен.
        // Для этого сгенерировать два числа, и, пользуюсь таблицей, вывести получившееся имя с эпитетом
        System.out.println("");
        System.out.println("Задание №8");

        int randomVal = (int) Math.round(Math.random() * 9);
        if (randomVal == 0) {
            System.out.println("РАДИОАКТИВНЫЙ");
        } else if (randomVal == 1) {
            System.out.println("ГРЕЧНЕВЫЙ");
        } else if (randomVal == 2) {
            System.out.println("ДЕМОНИЧЕСКИЙ");
        } else if (randomVal == 3) {
            System.out.println("ПРИЗРАЧНЫЙ");
        } else if (randomVal == 4) {
            System.out.println("ОЗОРНОЙ");
        } else if (randomVal == 5) {
            System.out.println("НАВЯЗЧИВЫЙ");
        } else if (randomVal == 6) {
            System.out.println("КОСМИЧЕСКИЙ");
        } else if (randomVal == 7) {
            System.out.println("ЗВЕЗДНЫЙ");
        } else if (randomVal == 8) {
            System.out.println("СЕКСУАЛЬНЫЙ");
        } else if (randomVal == 9) {
            System.out.println("НЕПОБЕДИМЫЙ");
        }


        randomVal = (int) Math.round(Math.random() * 10);
        switch (randomVal) {
            case 0:
                System.out.println("КАПИТАН");
                break;
            case 1:
                System.out.println("ЭЛЬФ");
                break;
            case 2:
                System.out.println("ПИНГВИН");
                break;
            case 3:
                System.out.println("ИНДЕЕЦ");
                break;
            case 4:
                System.out.println("ГНОМ");
                break;
            case 5:
                System.out.println("УПЫРЬ");
                break;
            case 6:
                System.out.println("БОРОДАЧ");
                break;
            case 7:
                System.out.println("БОБЕР");
                break;
            case 8:
                System.out.println("КОРОЛЬ");
                break;
            case 9:
                System.out.println("ТОЛСТОПУЗ");
                break;
            case 10:
                System.out.println("КИЛЛЕР");
                break;
        }
    }
}
