package Lesson12;

public class Person {
    String name;
    int age;
    int phone;
    String country;
    String city;
    String street;

    Person(String name, int age, String country, String city, String street) {
        this.name = name;
        this.country = country;
        this.city = city; // конструктор
        this.age=age;
        this.street=street;
    }

    private void printCountry() {
        System.out.print(country + "\t");
    } //метод

    private void printCity() {
        System.out.print(city + "\t");
    } //метод

    public void printAddress() {
        System.out.print("Адрес:  " );
        printCountry();
        printCity();
        System.out.print(street);

    } //метод
}

