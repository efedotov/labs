package Lesson4;
//Используя логические операции, вывести в консоль таблицы истинности для логических операций И,
// ИЛИ, ИСКЛЮЧАЮЩЕЕ ИЛИ, НЕ, И-НЕ, ИЛИ-НЕ (последние две - отрицание результата И и ИЛИ, соответственно)

public class Practice4_1 {
    public static void main(String args[]) {
        System.out.println("Логическая операция И:");
        System.out.println("true  & true  = " + (true & true));
        System.out.println("false & false = " + (false & false));
        System.out.println("true  & false = " + (true & false));
        System.out.println("false & true  = " + (false & true));
        System.out.println("----------------------------------------");
        System.out.println("Логическая операция ИЛИ:");
        System.out.println("true  | true  = " + (true | true));
        System.out.println("false | false = " + (false | false));
        System.out.println("true  | false = " + (true | false));
        System.out.println("false | true  = " + (false | true));
        System.out.println("----------------------------------------");
        System.out.println("Логическая операция ИСКЛЮЧАЮЩЕЕ ИЛИ:");
        System.out.println("true  ^ true  = " + (true ^ true));
        System.out.println("false ^ false = " + (false ^ false));
        System.out.println("true  ^ false = " + (true ^ false));
        System.out.println("false ^ true  = " + (false ^ true));
        System.out.println("----------------------------------------");
        System.out.println("Логическая унарная операция НЕ :");
        System.out.println("!true  = " + (!true));
        System.out.println("!false = " + (!false));
        System.out.println("----------------------------------------");
        System.out.println("Логическая операция И-НЕ :");
        System.out.println("true  & true  = " + !(true & true));
        System.out.println("false & false = " + !(false & !false));
        System.out.println("true  & false = " + !(true & false));
        System.out.println("false & true  = " + !(false & true));
        System.out.println("----------------------------------------");
        System.out.println("Логическая операция ИЛИ-НЕ :");
        System.out.println("true  | true  = " + !(true | true));
        System.out.println("false | false = " + !(false | false));
        System.out.println("true  | false = " + !(true | false));
        System.out.println("false | true  = " + !(false | true));
    }
}
