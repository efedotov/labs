package Lesson4;
// Написать условие (выражение, результат которого - boolean, зависящее от одной переменной типа int),
// которое истинно, если третий и седьмой бит - 1, а второй и пятый - 0, а также истинно,
// если переменная равна 100, или равна 500, или равна 0.

import java.util.Scanner;

public class Practice4_3 {
    public static void main(String[] args) {
int a = 0b11001010;
        System.out.println(String.format("Сдвиг вправо на 3: %32s", Integer.toBinaryString(a >> 3)));
        System.out.println(String.format("Сдвиг вправо на 7: %32s", Integer.toBinaryString(a >> 7)));
        System.out.println(String.format("Сдвиг вправо на 5: %32s", Integer.toBinaryString(a >> 5)));
        System.out.println(String.format("Сдвиг вправо на 2: %32s", Integer.toBinaryString(a >> 2)));
       // исправленная версия --> d
        boolean d = ((((a >> 3)&1)==1)&&(((a>>7)&1)==1)) && ((((a >> 5)&1)==0)&&(((a >> 2)&1)==0)) || (a==100 || a==500 || a==0);
        System.out.println("Result= " + d);

        // Тернарный оператор
        // Напишите выражение, значение которого равно заданному числу i, если число отрицательно,
        // и равно –i, если число i неотрицательно
        Scanner scan = new Scanner(System.in);
        System.out.print("Введите любое число: ");
        int s = scan.nextInt();
        int x;
        int i;
       i = (s<0) ? (x=s) : (x=-s);
        System.out.println("Ответ: " + x);


       // Напишите выражение, которое проверяет, делится ли целое число без остатка на 11
        System.out.println("Введите любое число: ");
        int h = scan.nextInt();
        boolean y = (h%11==0);
        System.out.println("Ответ: " + y);

        //Напишите логическое выражение которое истинно, если заданное целое число четно, но при этом не делится на 4 и на 3.
        System.out.println("Введите любое число: ");
        int g = scan.nextInt();
        boolean z = (g%2==0)&&((g%3!=0)&&(g%4!=0));
        System.out.println("Ответ: " + z);
    }
}
