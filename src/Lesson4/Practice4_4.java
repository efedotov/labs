package Lesson4;
import java.util.Scanner;
// Предположим, дано пятизначное число. Вывести пятую цифру этого числа
// (считая с единиц, то есть пятая цифра это число десятков тысяч).
// Расширить алгоритм до того, чтобы можно было задавать позицию, с которой нужно вывести цифру.

public class Practice4_4 {
    public static void main(String[] args) {
int a = 78532;
        System.out.print("Введите позицию, с которой нужно вывести цифру. Допустимые значения от 1 до 5: ");
        Scanner scan = new Scanner(System.in);
        int p = scan.nextInt();
        switch (p) {
            case 1:
                System.out.println("Разряд единиц:" + a % 10);
                break;
            case 2:
                System.out.println("Разряд десятков:" + a % 100/10);
                break;
            case 3:
                System.out.println("Разряд сотен:" + a % 1000/100);
                break;
            case 4:
                System.out.println("Разряд тысяч:" + a % 10000/1000);
                break;
            case 5:
                System.out.println("Разряд десятков тысяч:" + a/10000);
                break;
        }
        // исправленная версия
        int x = 87915;
        System.out.print("Введите позицию, с которой нужно вывести цифру. Допустимые значения от 1 до 5: ");
        scan = new Scanner(System.in);
        int n = scan.nextInt();
        System.out.print("Digit: " + (x/(int)Math.pow(10,n-1)%10));
       }
}

