package Lesson4;
// Даны 4 переменные a, b, c, d. Написать условие (выражение, результат которого – boolean),
// которое истинно, если а делится на 2 и b делится на 4, либо с делится на 3, а d не делится на 3.

public class Practice4_2 {
    public static void main(String[] args) {
        int a = 6;
        int b = 12;
        int c = 9;
        int d = 11;
         // исправленная версия --> e
        boolean e = (((a%2)==0 && (b%4)==0)) || (((c%3)==0 && (d%4)!=0));
        System.out.println("Result= " +e);
    }
}
