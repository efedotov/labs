package Lesson21;

interface Printable {

    public void printDoc();
    public void setData(String data);
    public void setSize (double size);
}
