package Lesson21;

import java.util.Date;

public class BinaryDocument extends Document implements Runnable {
    private byte data;

    public BinaryDocument(String name, double size) {
        super(name, size);
    }

    public void setData(byte data) {
        this.data = data;
    }

    public void run() {
        System.out.println("Двоичный документ можно запустить:");
        System.out.println("And contrariwise, what it is, it wouldn't be\n" +
                "And what it wouldn't be, it would, you see?");
        System.out.println(creationDate);

    }
}

