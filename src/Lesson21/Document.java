package Lesson21;

import java.util.Date;

public class Document implements Readable, Writable {
    private String name;
    Date creationDate;
    private double size;

    public Document(String name, double size) {
        this.name = name;
        this.size = size;
        this.creationDate = new Date(System.currentTimeMillis());
    }

    public void setSize(double size) {
        this.size = size;
    }

    public void readDoc()
    {
        System.out.println("Любой документ можно прочитать");
    }

    public void writeDoc() {
        System.out.println("В любой документ можно сделать запись");
          };

}
