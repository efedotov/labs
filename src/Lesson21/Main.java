package Lesson21;

public class Main {
    public static void main(String[] args) {
        Document document1 = new Document("General document1", 15);
        Writable document2 = new Document("General document2", 100);
        Printable text1 = new TextDocument("Text document1", 20);
        Runnable binary1 = new BinaryDocument("Binary document1", 10);
        //Printable binaryPrintable = new BinaryDocument("Binary document2", 60);

        text1.setSize(25.8);
        text1.setData("If I had a world of my own, everything would be nonsense.\n" +
                "Nothing would be what it is because everything would be what it isn't.");

        binary1.setSize(5.28);
        binary1.setData((byte) 0b011);

        // чтение - доступно для всех
        document1.readDoc();
        ((TextDocument) text1).readDoc();
        ((BinaryDocument) binary1).readDoc();
        System.out.println();

        // запись - доступна для всех
        document2.writeDoc();
        ((TextDocument)text1).writeDoc();
        ((BinaryDocument) binary1).writeDoc();
        System.out.println();


// печать
        text1.printDoc();

    }
}