package Lesson7;
// Напишите игру Камень-Ножницы-Бумага-Ящерица-Спок. Игрок вводит число от 0 до 4 обозначающее один из 5 вариантов.
// Далее необходимо вывести оба выбора словам а не цифрами.
// А также написать победил ли игрок или нет или была ничья. Игра продолжается до тех пор пока игрок не введет цифру 9.
// https://bigbangtheory.fandom.com/wiki/Rock,_Paper,_Scissors,_Lizard,_Spock

import java.util.Scanner;

public class Practice7_2 {
    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);
        int userChoice = 0;

        while (userChoice != 9) {
            System.out.println("Please enter values from 0 to 4. If you want to quit the game, enter 9:");
            userChoice = scanner.nextInt();
            if (userChoice == 9) break;
            int compValue = (int) Math.round(Math.random() * 4);

            int value = userChoice;
            System.out.println("Your value is: ");
            word(value);
            value = compValue;
            System.out.println("Opponent's value is: ");
            word(value);

            double x = userChoice;
            double y = compValue;
            double z = 0;
            z = (x - y + 5) % 5;

            if (z == 0) {
                System.out.println("It is tie");
            } else if (z >= 3) {
                System.out.println("You have failed");
            } else {
                System.out.println("You have won");
            }
        }
        System.out.println("The game is over");
    }

    public static int word(int value) {
        if (value == 0) {
            System.out.println("Rock");
        } else if (value == 1) {
            System.out.println("Spock");
        } else if (value == 2) {
            System.out.println("Paper");
        } else if (value == 3) {
            System.out.println("Lizard");
        } else if (value == 4) {
            System.out.println("Scissors");
        }
        return value;
    }

}





