package Lesson7;

import java.util.Scanner;

public class Practice7_1 {
    public static void main(String args[]) {

        Scanner scanner = new Scanner(System.in);
        int userChoice = 0;
        while (userChoice!=9) {
            System.out.println("Please enter values from 0 to 4. If you want to quit the game, enter 9:");
            userChoice = scanner.nextInt();

            if (userChoice == 9) break;
            if (userChoice == 0) {
                System.out.println("Your value is Lizard");
            } else if (userChoice == 1) {
                System.out.println("Your value is Scissors");
            } else if (userChoice == 2) {
                System.out.println("Your value is Rock");
            } else if (userChoice == 3) {
                System.out.println("Your value is Spock");
            } else if (userChoice == 4) {
                System.out.println("Your value is Paper");
            }

            int compValue = (int) Math.round(Math.random() * 4);

            if (compValue == 0) {
                System.out.println("Opponent's value is Lizard");
            } else if (compValue == 1) {
                System.out.println("Opponent's value is Scissors");
            } else if (compValue == 2) {
                System.out.println("Opponent's value is Rock");
            } else if (compValue == 3) {
                System.out.println("Opponent's value is Spock");
            } else if (compValue == 4) {
                System.out.println("Opponent's value is Paper");
            }

            if (userChoice==compValue) {
                System.out.println("Draw");
            } else if ((userChoice==2)&&(userChoice>compValue)) {
                System.out.println("You have won");
            } else if ((userChoice==4)&&(compValue==2||compValue==3)) {
                System.out.println("You have won");
            } else if ((userChoice==1)&&(compValue==0||compValue==4)) {
                System.out.println("You have won");
            } else if ((userChoice==3)&&(compValue==1||compValue==2)) {
                System.out.println("You have won");
            } else if ((userChoice==0)&&(compValue==4||compValue==3)) {
                System.out.println("You have won");
            } else {
                System.out.println("You have lost");
            }
            continue;
        }
        System.out.println("The game is over");
    }
}
