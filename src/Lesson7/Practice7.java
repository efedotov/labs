package Lesson7;
// Необходимо вывести число фибоначчи двумя спобами, рекурсивно и в цикле. По желанию вывести время затраченно на каждый способ.
// Испльзуйте long currentTime = System.nanoTime(); для узнавания текущего времени в наносекундах

public class Practice7 {
    public static void main(String args[]) {
        int x = 0;
        int i = 1;
        System.out.println("Ряд Фибоначчи");

        long currentTime = System.nanoTime();
        for (int j = 1; j < 31; j++) {
            x += i; // current number
            i = x - i; //previous number
            System.out.print(" " + x);
        }
        System.out.println("");
        System.out.println("Число Фибоначчи циклом for:\t" + x);
        System.out.println("Время на вычисление числа Фибоначчи циклом for: \t" + (System.nanoTime() - currentTime));
        System.out.println("");

        long currentTime1 = System.nanoTime();
        int ii = 1;
        int xx = 0;
        int j = 1;
        do {
            xx += ii;
            ii = xx - ii;
            j++;
            System.out.print(" " + xx);
        } while (j < 31);
        System.out.println("");
        System.out.println("Число Фибоначчи циклом do while: \t" + xx);
        System.out.println("Время на вычисление числа Фибоначчи циклом do while: \t" + (System.nanoTime() - currentTime1));

        System.out.println("");
        long currentTime2 = System.nanoTime();
        System.out.println("Число Фибоначчи рекурсией \t" + fibo(30));
        System.out.println("Время на вычисление числа Фибоначчи рекурсией \t" + (System.nanoTime() - currentTime2));
    }

    public static long fibo(long i) {
        if (i == 0) {
            return 0;
        } else if (i == 1) {
            return 1;
        } else {
            return fibo(i - 1) + fibo(i - 2);
        }
    }
}
