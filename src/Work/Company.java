package Work;

public class Company {
    String address;
    String sphere;
    String companyName;
    Employee[] Employees;
    public Employee owner;

    public Company(String companyName, String address, String sphere, Employee[] Employees) {
        this.companyName = companyName;
        this.address = address;
        this.sphere = sphere;
        this.Employees = Employees;
    }

    public String getCompanyName(String companyName) {
        return companyName;
    }

    private static void findEmployee(String s, Employee[] Employees) {
        for (Employee element : Employees) {
            if (s.equals(element.getPosition())) {
                System.out.println("Имя сотрудника: " + element.getName() + "\n" + "Позиция сотрудника: " + "\t" + element.getPosition());
            }
        }
    }


    public Employee getOwner() {
        return owner;
    }


    protected static void findOwner(Company company) {
        System.out.println("Владелец " + company.companyName + "\t" + "-" + "\t" + company.getOwner().name);
    }


    public static void findCeo(Company company) {
        int i = 0;
        for (Employee element : company.Employees) {
            if ((element.getPosition()).equals("руководитель") || (element.getPosition()).equals("CEO") || (element.getPosition()).equals("директор")) {
                System.out.println("Руководитель " + company.companyName + "\t" + "=" + "\t" + element.getName());
            }
        }
    }

}