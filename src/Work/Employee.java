package Work;

public class Employee {
    String name;
    String position;
    int salary;

    public Employee(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getPosition() {
        return position;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public static void maxSalary(Company company) {
        int i = 0;
        int maxSalary = company.Employees[i].getSalary();
        for (Employee element : company.Employees) {
            if (maxSalary < element.getSalary()) {
                maxSalary = element.getSalary();
            }
            i++;
        }
        System.out.println("Максимальная зарплата в " + company.companyName + "\t" + "=" + "\t " + maxSalary);
    }


    public static void averageSalary(Company company) {
        int y = 0;
        double sumSalary = 0;
        for (Employee element : company.Employees) {
            sumSalary = (element.getSalary() + sumSalary);
            y++;
        }
        double averageSalary = sumSalary / y;
        System.out.println("Средняя зарплата в " + company.companyName + "\t" + "=" + "\t " + averageSalary);
    }

}