package Lesson11;

public abstract class Main {
    public Main() {
    }

    public static void main(String[] args) {
        // Создайте пару адресов и выведите их на экран
        Address vika = new Address("USA", "New York", "105-th Avenue", 25, 145, 123456);
        Address natasha = new Address("Great Britain", "London", "Regent street", 16, 35);

        System.out.println("Адрес Вики: \n country: " + vika.country + ",\t city: " + vika.city + ",\t street:" + vika.street + ",\t house:" + vika.house + ",\t flat:" + vika.flat + ",\t postalCode:" + vika.postalCode);
        System.out.println();
        System.out.println("Адрес Наташи: \n country: " + natasha.country + ",\t city: " + natasha.city + ",\t street:" + natasha.street + ",\t house:" + natasha.house + ",\t flat:" + natasha.flat);
        System.out.println();

        // Создайте одну или несколько семей
        Person mother = new Person("Анна", "Владимировна", 1980);
        Person father = new Person("Василий", "Евгеньевич", 1976);
        Person dog = new Person("Майкл", 2015);

        Partner husband = new Partner("Василий", 43, "15 лет");
        Partner wife = new Partner("Анна", 39, "10 лет");

        Address myHome = new Address("Russia", "SPb", "Nevsky pr", 15, 56, 16589);

        String[] children = new String[]{"Наташа", "Андрей", "Дина", "Ева"};

        Person.printFamily(mother, father, dog, myHome, children);
        System.out.println();
        Person.married(wife, husband);
    }
}





