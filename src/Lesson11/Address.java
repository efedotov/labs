package Lesson11;

public class Address {
    String country;
    String city;
    String street;
    int house;
    int flat;
    int postalCode;

    Address(String country, String city, String street, int house, int flat, int postalCode) {
        this.country = country;
        this.city = city;
        this.street = street;
        this.house = house;
        this.flat = flat;
        this.postalCode = postalCode;
    }

    Address(String country, String city, String street, int house, int flat) {
        this.country = country;
        this.city = city;
        this.street = street;
        this.house = house;
        this.flat = flat;
    }

}
