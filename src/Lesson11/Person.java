package Lesson11;

public class Person {
    final String familyName = "Прокопенко";
    String patronymic;
    String givenName;
    int dateOfBirth;
    Address myHome;
    Partner wife;
    Partner husband;

    Person(String givenName, String patronymic, int dateOfBirth) {
        this.patronymic = patronymic;
        this.givenName = givenName;
        this.dateOfBirth = dateOfBirth;
    }

    Person(String givenName, int dateOfBirth) {
        this.givenName = givenName;
        this.dateOfBirth = dateOfBirth;
    }

        public static void printFamily (Person mother, Person father, Person dog, Address myHome, String[] children) {
        System.out.println("Семья");
        System.out.println ("Мама: " + mother.givenName + "\t" + mother.patronymic + "\t" + mother.familyName + "\t" + mother.dateOfBirth);
        System.out.println ("Папа: " + father.givenName + "\t" + father.patronymic + "\t" + father.familyName + "\t" + father.dateOfBirth);
        System.out.print("Дети: \t");

        for(String element: children) {
            System.out.print(element + ";\t");
        }

        System.out.println();
        System.out.println("Собака: " + dog.givenName + "\t" + dog.dateOfBirth);
        System.out.println("Адрес: " + myHome.country + ",\t" + myHome.city + ",\t" + myHome.street + ",\t" + myHome.house);
    }

    public static void married(Partner wife, Partner husband) {
        System.out.println("Василий " + wife.term + " женат на " + wife.name + ", которой " + wife.age + " лет");
        System.out.println("Анна " + husband.term + " замужем за " + husband.name + ", которому " + husband.age + " лет");
    }


}


