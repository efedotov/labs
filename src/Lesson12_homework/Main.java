package Lesson12_homework;

public class Main {
    public static void main(String[] args) {
        Address billHomeAddress = new Address("Austria", "Vienna", "Ring", 5050);
        Address georgeHomeAddress = new Address("Slovakia", "Bratislava");
        Address michaelHomeAddress = new Address("Italy", "Rome");

        Address officeAddress = new Address("Austria", "Vienna", "HerzogStrasse", 333);
        Address michaelOfficeAddress = new Address("Austria");

        Person bill = new Person("Bill", 40, billHomeAddress, officeAddress);
        Person george = new Person("George", 45, georgeHomeAddress, officeAddress);
        Person michael = new Person("Michael", michaelOfficeAddress);

        // в классе Address должен быть метод выводящий на экран полностью
        // весь адрес (если какая-то информация не заполнена, то не выводить ее на экран)
        System.out.println("Домашний адрес Джорджа: ");
        Address.prAddress(georgeHomeAddress);

        System.out.println("Домашний адрес Билла: ");
        Address.prAddress(billHomeAddress);

        // методы, которые доступны только из класса Person для вывода на экран домашнего адреса;
        // Person.printHomeAddress(michaelHomeAddress);

        // Вывести на экран информацию, где работает каждый человек.
        System.out.println("Рабочие адреса: ");
        Person.printOfficeAddress(bill, officeAddress);
        Person.printOfficeAddress(george, officeAddress);
        Person.printOfficeAddress(michael, michaelOfficeAddress);
    }
}
