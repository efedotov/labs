package Lesson12_homework;

public class Person {
String name;
int age;
Address homeAddress;
Address officeAddress;
Person person;

public Person (String name, int age, Address homeAddress, Address officeAddress) {
    this.name = name;
    this.age = age;
    this.homeAddress = homeAddress;
    this.officeAddress = officeAddress;
}

public Person (String name, Address officeAddress) {
    this.name = name;
    this.officeAddress = officeAddress;
}

private static void printHomeAddress (Address homeAddress) {
    System.out.println("Домашний адрес:" + homeAddress.country + homeAddress.city + homeAddress.street + homeAddress.postcode);
}

public static void printOfficeAddress (Person person, Address address) {
    System.out.print(person.name + "\t");
    if (person.age!=0) {
        System.out.print(person.age + " лет");
    }
    System.out.println();
    System.out.print("Рабочий адрес: \t");
    Address.prAddress(address);
    }

}
