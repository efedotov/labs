package Lesson12_homework;

public class Address {
    String country;
    String city;
    String street;
    int postcode;

    public Address(String country, String city, String street, int postcode) {
        this.country = country;
        this.city = city;
        this.street = street;
        this.postcode = postcode;
    }

    public Address(String country, String city) {
        this.country = country;
        this.city = city;
    }

    public Address(String country) {
        this.country = country;
    }

    public static void prAddress(Address address) {
        if (address.country != null) {
            System.out.print(address.country + "\t");
        }
        if (address.city != null) {
            System.out.print(address.city + "\t");
        }
        if (address.street != null) {
            System.out.print(address.street + "\t");
        }
        if (address.postcode != 0) {
            System.out.print(address.postcode);
        }
        System.out.println();
        System.out.println();
    }

}