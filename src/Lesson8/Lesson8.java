package Lesson8;

public class Lesson8 {
    public static void main(String[] args) {
// задание 1
        System.out.println("Задание №1");
        System.out.println(politeMethod("Alice"));
        System.out.println("");

// задание 2
        System.out.println("Задание №2");
        System.out.println(WrapWord("Winter", "****"));
        System.out.println("");

        // посередине
        System.out.println(WrapWord1("Winter", "****"));
        System.out.println("");

// задание 3
        System.out.println("Задание №3");
        System.out.println((half("Nice to meet you!")));
        System.out.println("");

// задание 4
        System.out.println("Задание №4");
        System.out.println((endsOod("Made of wood")));
        System.out.println("");

// задание 5
        System.out.println("Задание №5");
        System.out.println("Количество символов е: " + (charCounter("eensy weensy spider", 'e')));
        System.out.println("");


// задание 6
        System.out.println("Задание №6");
        System.out.println("Нечетное? " + (oddLength(13587.123)));
        System.out.println("");


// задание 7
        System.out.println("Задание №7");
        System.out.println("Слово 'java' встречается: " + (stringCounter("Java programmers love Java", "java")) + " раза");
    }



    public static String politeMethod(String name) {
        return "Hello " + name + '!';
    }

    public static String WrapWord(String word, String wrap) {
        return "" + wrap.charAt(0) + word + wrap.charAt(1) + wrap.charAt(2) + wrap.charAt(3);
    }

    public static String WrapWord1 (String word, String wrap) {
        return "" + wrap.charAt(0) + wrap.charAt(1) + word + wrap.charAt(2) + wrap.charAt(3);
    }


    public static String half(String s) {
        return s.substring(0, s.length() / 2);
    }

    public static boolean endsOod(String s) {
        String newStr = s.substring(s.length() - 3);
        String model = "ood";
        if (newStr.equals(model))
            return true;
        else return false;
    }

    public static int charCounter(String s, char c) {
        int i = 0;
        for (int j = 0; j < s.length(); j++) {
            if (s.charAt(j) == c)
                i++;
        }
        return i;
    }

    public static boolean oddLength(double d) {
        String z = String.valueOf(d);
        System.out.println("Количество символов: " + z.length());
        return z.length() % 2 != 0;
    }

    public static int stringCounter(String s, String searchString) {
        String m = s.toLowerCase();
        String n;
        // System.out.println("Длина всей строки " + m.length());
        int f = 0;
        while (m.length() != 0) {
            if (m.endsWith(searchString)) {
                n = m.substring(0, m.length() - searchString.length());
                m = n;
               // System.out.println("Оствшаяся строка if true " + m);
                f++;
            } else {
                n = m.substring(0, m.length() - 1);
                m = n;
               // System.out.println("Оствшаяся строка if false " + m);
            }
        }
        return f;
    }
}










