package Lesson13;

public class Main {
    public static void main(String args[]) {
        Horse spark = new Horse ("Spark");
        Horse charley = new Horse ("Charley");

        spark.setColour("brown");
        charley.setColour("white");
        charley.setAge(15);
        charley.setWeight(90.5);
        spark.setAge(9);
        spark.setWeight(80.153);
        spark.print();
        System.out.println();
        charley.print();

        //System.out.println(spark.getName() + "\n " +  charley.age + "\n " + charley.weight);


    }
}
