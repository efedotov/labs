package Lesson17;

public class Cat extends Animal {
    public String sound = "Meow";

    Cat(String name) {
        super(name);
    }

    //  void setMeow(String sound) {
    //      this.sound = sound;
    //  }

    @Override
    void makeSound() {
        System.out.println("Животное говорит: " + sound);
    }

}