package Lesson17;

public class Dog extends Animal {
  //  private static final String SOUND = "Bark";
  public String sound = "Bark";

    Dog(String name) {
        super(name);
    }

  //  void setBark(String sound) {
    //    this.sound = sound;
    //}

    @Override
    void makeSound() {
        System.out.println("Животное говорит: " + sound);
    }
}
