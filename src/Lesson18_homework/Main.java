package Lesson18_homework;

public class Main {
    public static void main(String[] args) {

        Report shortReport = new Report("Summary");
        TimestampedReport dailyReport = new TimestampedReport("Morning newsletter");
        UsernameReport file = new UsernameReport("Dossier");
        TimestampedReport weeklyReport = new TimestampedReport("Monday newsletter");
        Report longReport = new Report("Story");

        Reporter.report(shortReport);
        Reporter.report(dailyReport);
        Reporter.report(file);
        Reporter.report(longReport);
        Reporter.report(weeklyReport);
    }
}
