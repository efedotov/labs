package Lesson18_homework;

public class Report {
    String reportData;
    static int i = 0;
    int reportCount;

    protected Report(String reportData) {
        this.reportData = reportData;
        i++;
        reportCount = i;
    }

    public String getReport() {
        return reportCount + ": " + reportData;
    }
}
