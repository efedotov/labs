package Lesson18_homework;

import java.util.Date;

public class TimestampedReport extends Report {
    Date t;

    TimestampedReport(String name) {
        super(name);
        this.t = new Date(System.currentTimeMillis());
    }

    @Override
    public String getReport() {
        super.getReport();
        return reportCount + ": " + t + ": " + reportData;
    }
}
