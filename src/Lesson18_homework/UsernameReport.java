package Lesson18_homework;

public class UsernameReport extends Report {

    UsernameReport(String name) {
        super(name);
    }

    @Override
    public String getReport() {
        super.getReport();
        String userName = System.getProperty("user.name");
        return reportCount + ": " + userName + ": " + reportData;
    }
}
