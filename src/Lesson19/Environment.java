package Lesson19;

import Lesson19.goods.vegetables.*;
import Lesson19.goods.forestGift.*;
import Lesson19.packet.Packet;
import Lesson19.users.User;
import stub.Simulator;
import test.*;
import Lesson19.robot.Terminator;
import Lesson19.jars.*;
import Lesson19.cupboards.*;

public class Environment {
    public static void main(String[] args) {

        // импорт
        System.out.println(TestCase.startTestCase());
        System.out.println(TestChain.startTestChain());
        System.out.println(Simulator.startSimulator());
        System.out.println();

        // Иерархия пакетов
        Lesson19.animal.Cat.sayHello();
        Lesson19.animal.Dog.sayHello();
        Terminator.sayHello();
        System.out.println();

        // Иерархия пакетов II
        Cucumber cucumber1 = new Cucumber("cucumber1", "Zozyla", "Poland");
        Cucumber cucumber2 = new Cucumber("cucumber2", "Murashka", "Israel");
        Tomato tomato1 = new Tomato("tomato1", "Bull heart", "XXL");
        Tomato tomato2 = new Tomato("tomato2", "Rapunzel", "XS");
        Mushroom mushroom1 = new Mushroom("mushroom1", "chanterelle", true);
        Mushroom mushroom2 = new Mushroom("mushroom2", "fly agaric", false);
        Berry berry1 = new Berry("berry1", "strawberry", 50);
        Berry berry2 = new Berry("berry2", "blueberry", 150);

        Object[] jar1 = new Object[8];
        Object[] jar2 = new Object[4];
        Object[] jar3 = new Object[3];
        Object[] jar4 = new Object[5];


        // кладем продукты по банкам
        System.out.println("Jar1 contains following products:");
        Jar.canning(jar1, cucumber1);
        Jar.canning(jar1, cucumber2);
        Jar.canning(jar1, tomato1);
        Jar.canning(jar1, tomato2);
        Jar.canning(jar1, mushroom1);
        Jar.canning(jar1, berry1);
        Jar.canning(jar1, berry2);
        System.out.println();

        System.out.println("Jar2 contains following products:");
        Jar.canning(jar2, cucumber1);
        Jar.canning(jar2, tomato1);
        Jar.canning(jar2, mushroom1);
        System.out.println();

        System.out.println("Jar3 contains following products:");
        Jar.canning(jar3, mushroom2);
        System.out.println();

        //  вынимаем продукты из банки
        System.out.println("Из банки можно достать продукты!");
        Jar.lookForProduct(jar1, cucumber1);
        Jar.lookForProduct(jar1, mushroom2);
        Jar.lookForProduct(jar2, tomato1);
        Jar.lookForProduct(jar2, berry1);
        System.out.println();

        // Пользователь ест продукты
        System.out.println("Пользователь может доставать и есть продукты из банки!");
        User.eat(jar1, berry1);
        User.eat(jar2, tomato1);
        User.eat(jar2, cucumber2);
        System.out.println();

       // определяем фиксированное количество мест в шкафу
        Object [][] cupboard1 = new Object [3][4];
        System.out.println("Количество полок " + cupboard1.length);
        System.out.println("Количество мест на каждой полке " + cupboard1[0].length);
        System.out.println();

        // Ставим банки в шкаф
        System.out.println("Банки можно ставить в шкаф!");
        Сupboard.putJar(cupboard1,jar1);
        Сupboard.putJar(cupboard1,jar2);
        Сupboard.putJar(cupboard1,jar3);
        Сupboard.putJar(cupboard1,jar4);
        Сupboard.putJar(cupboard1,jar4);
        Сupboard.putJar(cupboard1,jar4);

        // Вынимаем банки из шкафа
        System.out.println();
        System.out.println("Банки можно вынуть из шкафа!");
        Сupboard.takeOutJar(cupboard1,jar1);
        Сupboard.takeOutJar(cupboard1,jar4);
        Сupboard.takeOutJar(cupboard1,jar2);
        System.out.println();
        Сupboard.print(cupboard1);

        // Складываем пакеты
        System.out.println();
        System.out.println("Пакеты можно положить в пакет!");
       }
}
