package Lesson19.cupboards;

public class Сupboard {
    static int row = 0;
    static int column = 0;
    static int jarAmount = 0;
    static int jarNumber = 0;

    public static void putJar(Object[][] cupboard, Object[] jar) {
        if (cupboard[row][column] == null) {
            cupboard[row][column] = jar;
            column++;
            if (column == cupboard[0].length) {
                row++;
                column = 0;
                cupboard[row][column] = jar;
                column++;
            }
            jarAmount++;
            System.out.println("Вы поставили в шкаф " + jarAmount + "-ю банку");
        }
    }


    public static void takeOutJar(Object[][] cupboard, Object[] jar) {
        for (Object[] innerArray : cupboard) {
            for (Object element : innerArray) {
                if (element != null) {
                    if (element.equals(jar)) {
                        jarNumber++;
                        System.out.println("Вы достали " + jarNumber + "-ю банку");
                    }
                }
            }
        }
    }

    public static void print (Object[][] cupboard) {
        for (Object[] innerArray : cupboard) {
            for (Object element : innerArray) {
                 System.out.println(element);
                    }
                }
            }
        }

