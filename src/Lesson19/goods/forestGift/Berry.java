package Lesson19.goods.forestGift;
import Lesson19.goods.Product;

public class Berry extends Product {
    int amount;

    public Berry (String name, String sort, int amount) {
        super(name,sort);
        this.amount = amount;
    }
}

