package Lesson19.goods.forestGift;
import Lesson19.goods.Product;

public class Mushroom extends Product {
    boolean edible;

    public Mushroom (String name, String sort, boolean edible) {
        super(name, sort);
        this.edible = edible;
    }

}
