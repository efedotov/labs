package Lesson19.goods;

public class Product {
    public String name;
    public String sort;

    public Product(String name, String sort) {
        this.name = name;
        this.sort = sort;
    }
    public String getName() {
        return name;
    }

    public String getSort () {
        return sort;
    }

    }

