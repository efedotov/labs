package Lesson19.goods.vegetables;

import Lesson19.goods.Product;

public class Cucumber  extends Product {
    String countryOfOrigin;

    public Cucumber(String name, String sort, String countryOfOrigin) {
        super(name, sort);
        this.countryOfOrigin = countryOfOrigin;
    }

}
