package Lesson19.goods.vegetables;

import Lesson19.goods.Product;

public class Tomato extends Product {
    String size;

    public Tomato (String name, String sort, String size) {
       super(name, sort);
       this.size = size;
    }

}
