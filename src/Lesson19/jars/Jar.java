package Lesson19.jars;

import Lesson19.goods.Product;

public class Jar {
    public static int i = 0;

    // метод, который кладет в банку любые продукты.
    public static void canning(Object[] jar, Product product) {
        if (i >= jar.length) {
            i = 0;
        }
        if (jar[i] == null) {
            jar[i] = product;
            i++;
            System.out.println(i + ". " + product.getName() + " - " + product.getSort());
        }
    }

    // метод, который достает из банки один продукт.
    public static void lookForProduct(Object[] jar, Product product) {
        int j = 0;
        for (Object element : jar) {
            if (element != null) {
                if (element.equals(product)) {
                    System.out.println("Вы достали: " + product.name);
                } else {
                    j++;
                }
            } else {
                //  System.out.println(j);
                if (j == jar.length || jar[j] == null) {
                    System.out.println("Такого продукта в банке нет. ");
                }
            }
        }
    }

    // используется для пользователя, который хочет съесть продукт из банки (метод eat)
    public static boolean takeProduct(Object[] jar, Product product) {
        int j = 0;
        boolean z = false;
        for (Object element : jar) {
            if (element != null) {
                if (element.equals(product)) {
                    z = true;
                } else {
                    j++;
                }
            } else {
                if (j == jar.length || jar[j] == null) {
                    z = false;
                }
            }
        }
        return z;
    }
}






