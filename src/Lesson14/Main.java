package Lesson14;

public class Main {
    public static void main(String[] args) {
        BasicCat alice = new BasicCat("Alice");
        BasicCat ginger = new BasicCat("Ginger");
        BasicCat smoky = new BasicCat("Smoky");
        BasicCat lucky = new BasicCat("Lucky");
        BasicCat naughty = new BasicCat("Naughty");
        BasicCat fluffy = new BasicCat("Fluffy");

        alice.setAge(12);
        alice.setWeight(15.5);
        ginger.setAge(10);
        ginger.setWeight(12);
        smoky.setAge(0.5);
        smoky.setWeight(1.5);
        lucky.setAge(0.8);
        lucky.setWeight(1.2);
        naughty.setAge(0.3);
        naughty.setWeight(0.5);
        fluffy.setAge(0.3);
        fluffy.setWeight(0.3);

        // Добавить метод (НЕ в классе BasicCat), принимающий на вход экземпляр класса Cat
        // и печатающий возраст кошки, приведенный к условному “человеческому”.
        Calculation.humanAge(ginger);
        System.out.println();

        // Добавьте метод, печатающий количество созданных котят за всю историю существования класса.
        BasicCat.printAmountKittens();
        System.out.println();

        //Добавьте в класс BasicCat массив, который бы хранил ссылки на все когда-либо созданные экземпляры в статической переменной
        BasicCat.printAllKittens();
    }
}
