package Lesson14;

public class Cat {
    private String name;
    private int age;

    private static int countCat;
    private int catCounter;

    public Cat (String name, int age) {
        this.name = name;
        this.age = age;
        countCat++;
        catCounter=countCat;
    }

    public Cat getHost() {
        return new Cat( "Snowflake", 8);
    }

    public static int getCatCount() {
        return countCat;
    }

    public static void setCountCat(int count) {
        countCat = count;
    }


}
