package Lesson14;

public class Calculation {
    private static final int COEFFICIENT = 7;

    public static void humanAge(BasicCat cat) {
        double x = cat.age * COEFFICIENT;
        System.out.println("Возраст " + cat.name + ", приведенный к человеческому = " + x);
    }
}
