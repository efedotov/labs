package Lesson14;
import java.util.Arrays;

public class BasicCat {
    String name;
    double age;
    double weight;
    static int numKittens;
    static BasicCat[] kittens = new BasicCat[2];
    static BasicCat[] KittensBackup;


    public BasicCat(String name) {
        this.name = name;
    }

    public void setAge(double age) {
        this.age = age;
        if (age < 1) {
            numKittens++;
            addKittens(this);
        }
    }


    public void setWeight(double weight) {
        this.weight = weight;
    }


    public static void addKittens(BasicCat cat) {
        if (kittens[0] == null) {
            {
                kittens[0] = cat;
            }
        } else {
            KittensBackup = new BasicCat[kittens.length];
            KittensBackup = Arrays.copyOf(kittens, kittens.length);
            kittens = new BasicCat[KittensBackup.length + 1];
            for (int i = 0; i < KittensBackup.length; i++) {
                kittens[i] = KittensBackup[i];
            }
            kittens[kittens.length - 1] = cat;
        }
    }


    public static void printAmountKittens() {
        System.out.println("Количество котят: " + numKittens);
    }


    public static void printAllKittens() {
        System.out.println("Kittens:");
        for (BasicCat element : kittens)
            if (element != null) {
                System.out.println("Kitten's name " + element.name + ", " + "kitten's age " + element.age + ", " + "kitten's weight " + element.weight);
            }
    }
}

